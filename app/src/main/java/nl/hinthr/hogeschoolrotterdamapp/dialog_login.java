package nl.hinthr.hogeschoolrotterdamapp;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nl.hinthr.hogeschoolrotterdamapp.Client.Login;
import nl.hinthr.hogeschoolrotterdamapp.SharedPreferencesUtil.Session;
import nl.hinthr.hogeschoolrotterdamapp.model.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class dialog_login extends AppCompatActivity {

    @BindView(R.id.email)                AutoCompleteTextView email;
    @BindView(R.id.password)             EditText             password;
    @BindView(R.id.email_sign_in_button) Button               emailSignInButton;
    @BindView(R.id.cancel)               Button               cancel;
    @BindView(R.id.login_progress)       ProgressBar          loginProgress;
    @BindView(R.id.email_login_form)     LinearLayout         emailLoginForm;
    @BindView(R.id.login_form)           ScrollView           loginForm;
    Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_login);
        ButterKnife.bind(this);
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        session = new Session(dialog_login.this);
        // Specify the layout to use when the list of choices appears

    }

    @OnClick({R.id.email_sign_in_button, R.id.cancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.email_sign_in_button:
                if (!email.getText().toString().isEmpty() && !password.getText().toString().isEmpty()) {

                    Retrofit.Builder builder = new Retrofit.Builder()
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .baseUrl("http://145.24.222.157:8080/")
                            .addConverterFactory(GsonConverterFactory.create());
                    Retrofit retrofit = builder.build();

                    Login client = retrofit.create(Login.class);
                    Call<User> call2 = client.getLogin(email.getText().toString().trim(), password.getText().toString().trim());
                    call2.enqueue(new Callback<User >() {

                        @Override
                        public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {

                            String message_res = response.body() != null ? Objects.requireNonNull(response.body()).getMessage()  : null;
                            String loginname   = response.body() != null ? Objects.requireNonNull(response.body()).getUsername() : null;
                            String Key         = response.body() != null ? Objects.requireNonNull(response.body()).getKey()      : null;
                            if (response.isSuccessful()) {
                                session.setusename(loginname);
                                session.setuserkey(Key);
                                Toast.makeText(dialog_login.this, message_res+" "+session.getusename(), Toast.LENGTH_LONG).show();
                                finish();
                                startActivity(new Intent(getApplicationContext(),Mainactivity.class));
                            } else {
                                // error response, no access to resource?
                                Toast.makeText(dialog_login.this, "Error with connection, try again..", Toast.LENGTH_LONG).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            // something went completely south (like no internet connection)
                            Log.d("Error", t.getMessage());
                        }
                    });

                }

                break;
            case R.id.cancel:
                finish();

        }
    }
}
