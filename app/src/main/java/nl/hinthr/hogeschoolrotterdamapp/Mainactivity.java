package nl.hinthr.hogeschoolrotterdamapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nl.hinthr.hogeschoolrotterdamapp.SharedPreferencesUtil.Session;
import nl.hinthr.hogeschoolrotterdamapp.UserActivities.KlaslokaalRoosterdienst;
import nl.hinthr.hogeschoolrotterdamapp.UserActivities.MyReservationsClass;
import nl.hinthr.hogeschoolrotterdamapp.UserActivities.NotificationsLezen;
import nl.hinthr.hogeschoolrotterdamapp.UserActivities.QrCodeScanner;
import nl.hinthr.hogeschoolrotterdamapp.UserActivities.SearchClassroomInformation;
import nl.hinthr.hogeschoolrotterdamapp.UserActivities.SendANotification;




public class Mainactivity extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;


    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbarLayout;
    @BindView(R.id.app_bar)
    AppBarLayout appBar;

    ConstraintLayout container;

    @BindView(R.id.klsinfocardview)
    CardView klsinfocardview;
    @BindView(R.id.reserveringbekeijkencardview)
    CardView reserveringbekeijkencardview;
    @BindView(R.id.medlingversturencardview)
    CardView medlingversturencardview;
    @BindView(R.id.qrcodecardview)
    CardView qrcodecardview;
    @BindView(R.id.useraccount)
    CardView useraccount;
    @BindView(R.id.notificaties)
    CardView notificaties;
    @BindView(R.id.mainGrid)
    GridLayout mainGrid;
    @BindView(R.id.klaslokaalRoosterdienst)
    CardView klaslokaalRoosterdienst;
    @BindView(R.id.myname)
    TextView myname;

    private String result;
    public String thisname;
    Session session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainactivity);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        BackgroundWorker backgroundWorker = new BackgroundWorker(this);
        backgroundWorker.execute();
        session = new Session(Mainactivity.this); //in oncreate

        thisname = session.getusename();
        if (IsUserLoggedin()) {
            myname.setText(String.format("Welkom %s!", thisname));
        }


    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mainactivity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @OnClick(R.id.klsinfocardview)
    public void onButtonklaslokalenbekijkenClicked() {
        startActivity(new Intent(getApplicationContext(), SearchClassroomInformation.class));
    }

    @OnClick(R.id.reserveringbekeijkencardview)
    public void onButtonreserveringenbekijkenClicked() {

        if (IsUserLoggedin()==true) {
            startActivity(new Intent(getApplicationContext(), MyReservationsClass.class));
        } else {
            startActivity(new Intent(getApplicationContext(), dialog_login.class));
        }
    }

    @OnClick(R.id.medlingversturencardview)
    public void onButtonmeldingversturenClicked() {
        if (IsUserLoggedin()==true) {
            startActivity(new Intent(getApplicationContext(), SendANotification.class));
        } else {
            startActivity(new Intent(getApplicationContext(), dialog_login.class));
        }
    }

    @OnClick(R.id.qrcodecardview)
    public void onViewClicked() {
        if (IsUserLoggedin()==true) {
            startActivity(new Intent(getApplicationContext(), QrCodeScanner.class));
        } else {
            startActivity(new Intent(getApplicationContext(), dialog_login.class));
        }
    }

    @OnClick(R.id.useraccount)
    public void onUseraccountClicked() {
        if (IsUserLoggedin()==true) {
            startActivity(new Intent(getApplicationContext(), dialog_loggout.class));
        } else {
            startActivity(new Intent(getApplicationContext(), dialog_login.class));
        }
    }

    @OnClick(R.id.notificaties)
    public void onNotificatiesClicked() {
        if (IsUserLoggedin()==true) {
            startActivity(new Intent(getApplicationContext(), NotificationsLezen.class));
        } else {
            startActivity(new Intent(getApplicationContext(), dialog_login.class));
        }

    }


    @OnClick(R.id.klaslokaalRoosterdienst)
    public void onKlaslokaalRoosterdienstViewClicked() {
        startActivity(new Intent(getApplicationContext(), KlaslokaalRoosterdienst.class));

    }

    private boolean IsUserLoggedin() {
        if ( session.getusename().length() > 1 ||  session.getuserkey().length() > 1) {
            return true;
        } else {
            return false;
        }
    }
}



