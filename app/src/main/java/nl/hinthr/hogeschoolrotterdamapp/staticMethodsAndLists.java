package nl.hinthr.hogeschoolrotterdamapp;

import java.util.List;

import static java.util.Arrays.asList;

public class staticMethodsAndLists {
      public static  List<String> lesUren = asList("1", "2", "3","4","5","6","7","8","9","10","11","12","13","14","15");
      public static List<String> beginLes = asList("8:30", "9:20",  "10:30","11:20","12:10","13:00","13:50","15:00","15:50","16:40","17:50","18:40","19:30","20:20","21:10");
      public static List<String> eindLes = asList("9:20", "10:10", "11:20","12:10","13:00","13:50","14:40","15:50","16:40","17:50","18:40","19:30","20:20","21:10","22:00");
      private static String modifiedTimeOutput;

     public static String addDoublePoints(String time) {

        if (time.length() == 3) {
            modifiedTimeOutput = time.substring(0, 1) + ":" + time.substring(1, time.length());

        } else if (time.length() == 4) {
            modifiedTimeOutput = time.substring(0,2 ) + ":" + time.substring(2, time.length());
            System.out.println(time);

        }else {
            modifiedTimeOutput = time;
        }
        return modifiedTimeOutput;
    }


}
