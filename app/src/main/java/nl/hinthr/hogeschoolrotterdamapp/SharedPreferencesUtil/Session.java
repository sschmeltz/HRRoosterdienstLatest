package nl.hinthr.hogeschoolrotterdamapp.SharedPreferencesUtil;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Session {

    private SharedPreferences prefs;
    private SharedPreferences.Editor mSharedPreferencesEditor;
    public Session(Context cntx) {
        // TODO Auto-generated constructor stub
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
        mSharedPreferencesEditor = prefs.edit();
    }

    public void setusename(String usename) {
        prefs.edit().putString("usename", usename).apply();
    }
     public void setuserkey(String usekey) {
        prefs.edit().putString("usekey",   usekey).apply();
    }

     public void setbegintijd(String begintijd)  { prefs.edit().putString("setbegintijd",   begintijd).apply(); }
     public void seteindtijd (String eindtijd)   { prefs.edit().putString("seteindtijd",     eindtijd).apply(); }

    public String geteindtijd() {
        return prefs.getString("begintijd","");
    }
    public String getbegintijd() {
        return prefs.getString("begintijd","");
    }
    public String getusename() {
        return prefs.getString("usename","");
    }
    public String getuserkey() {
        return prefs.getString("usekey","");
    }
    public void removeKey(String key) {
        if (mSharedPreferencesEditor != null) {
            mSharedPreferencesEditor.remove(key);
            mSharedPreferencesEditor.commit();
        }
    }
}