package nl.hinthr.hogeschoolrotterdamapp;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nl.hinthr.hogeschoolrotterdamapp.Client.Login;
import nl.hinthr.hogeschoolrotterdamapp.SharedPreferencesUtil.Session;
import nl.hinthr.hogeschoolrotterdamapp.model.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class PoppupClass extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    @BindView(R.id.link_login)
    TextView linkLogin;
    @BindView(R.id.input_email)
    EditText inputEmail;
    @BindView(R.id.input_password)
    EditText inputPassword;
    @BindView(R.id.btn_signup)
    AppCompatButton btnSignup;
    @BindView(R.id.dropdowpoppup)
    Spinner dropdowpoppup;
    boolean notloggedInCheck;
    public static boolean loggedInCheck;
    Response<User> response;
    private User res;
    private String userName_res;
    private String message_res;
    public static String loginname ="" ;
    public  String Key ;
    Session session;

    boolean cancel = false;
    View focusView = null;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_poppup);
        ButterKnife.bind(this);
        //global variable
        session = new Session(PoppupClass.this);


        // onViewClicked(linkLogin, inputEmail,inputPassword, btnSignup);
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        notloggedInCheck = getIntent().getBooleanExtra("notloggedin", false);
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(PoppupClass.this, R.array.docentofstudent, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        dropdowpoppup.setAdapter(adapter1);


    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@hr.nl");
    }
    @OnClick(R.id.btn_signup)
    public void onViewClicked() {

        if (TextUtils.isEmpty(inputEmail.getText().toString().trim())) {
            inputEmail.setError(getString(R.string.error_field_required));
            focusView = inputEmail;
            cancel = true;
        }if (!isEmailValid(inputEmail.getText().toString().trim())) {
            inputEmail.setError(getString(R.string.error_invalid_email));
            focusView = inputEmail;
            cancel = true;
        }
        if (!inputEmail.getText().toString().isEmpty() && !inputPassword.getText().toString().isEmpty()) {

            Retrofit.Builder builder = new Retrofit.Builder()
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .baseUrl("http://145.24.222.157:8080/")
                    .addConverterFactory(GsonConverterFactory.create());
            Retrofit retrofit = builder.build();

            Login client = retrofit.create(Login.class);
            Call<User> call2 = client.getLogin(inputEmail.getText().toString().trim(), inputPassword.getText().toString().trim());
            call2.enqueue(new Callback<User >() {

                @Override
                public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {

                    message_res = response.body() != null ? Objects.requireNonNull(response.body()).getMessage()  : null;
                    loginname   = response.body() != null ? Objects.requireNonNull(response.body()).getUsername() : null;
                    Key         = response.body() != null ? Objects.requireNonNull(response.body()).getKey()      : null;
                    if (response.isSuccessful()) {
                        session.setusename(loginname);
                        session.setuserkey(Key);
                        Toast.makeText(PoppupClass.this, message_res+" "+session.getusename(), Toast.LENGTH_LONG).show();
                        //in oncreate
                         //and now we set sharedpreference then use this like




                        // user object available
                    } else {
                        // error response, no access to resource?
                        Toast.makeText(PoppupClass.this, "Error with connection, try again..", Toast.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    // something went completely south (like no internet connection)
                    Log.d("Error", t.getMessage());
                }
            });

        }

      /*  HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        // init cookie manager
        CookieHandler cookieHandler = new CookieManager();

        client = new OkHttpClient.Builder().addNetworkInterceptor(interceptor)
                .cookieJar(new JavaNetCookieJar(cookieHandler))
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());
                */
    }
}
