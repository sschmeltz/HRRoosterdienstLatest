package nl.hinthr.hogeschoolrotterdamapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nl.hinthr.hogeschoolrotterdamapp.SharedPreferencesUtil.Session;


public class dialog_loggout extends AppCompatActivity {
    Session session;

    @BindView(R.id.uitloggen) Button uitloggen;
    @BindView(R.id.cancel) Button cancel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_loggout);
        ButterKnife.bind(this);
        // sharedPreferences = getSharedPreferences("myprefs",Context.MODE_PRIVATE);
        //SharedPreferences.Editor editor = sharedPreferences.edit();
        session = new Session(getApplicationContext());

    }



    @OnClick(R.id.uitloggen)
    public void onEmailSignInButtonClicked() {
        session.removeKey("usename");
        session.removeKey("usekey");
        Toast.makeText(getApplicationContext(),"U bent uitgelogd",Toast.LENGTH_SHORT).show();
        finish();
        startActivity(new Intent(getApplicationContext(),Mainactivity.class));
    }



    @OnClick(R.id.cancel)
    public void onCancelClicked() {
        finish();


    }
}
