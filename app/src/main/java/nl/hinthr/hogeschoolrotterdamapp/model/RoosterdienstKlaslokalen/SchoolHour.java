
package nl.hinthr.hogeschoolrotterdamapp.model.RoosterdienstKlaslokalen;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SchoolHour {

    @SerializedName("uur")
    @Expose
    private String uur;
    @SerializedName("start")
    @Expose
    private String start;
    @SerializedName("eind")
    @Expose
    private String eind;

    public String getUur() {
        return uur;
    }

    public void setUur(String uur) {
        this.uur = uur;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEind() {
        return eind;
    }

    public void setEind(String eind) {
        this.eind = eind;
    }



}