
package nl.hinthr.hogeschoolrotterdamapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Notification {

    @SerializedName("notifications")
    @Expose
    private List<Notification_> notifications = null;
    @SerializedName("status")
    @Expose
    private Integer status;

    public List<Notification_> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification_> notifications) {
        this.notifications = notifications;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
