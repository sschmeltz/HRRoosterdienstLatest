
package nl.hinthr.hogeschoolrotterdamapp.model.RoosterdienstKlaslokalen;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RootsterdienstPOJO {

    @SerializedName("ElementName")
    @Expose
    private String elementName;
    @SerializedName("ElementType")
    @Expose
    private String elementType;
    @SerializedName("schoolId")
    @Expose
    private String schoolId;
    @SerializedName("lesson")
    @Expose
    private List<Lesson> lesson = null;

    public String getElementName() {
        return elementName;
    }
    public String getElementType() {
        return elementType;
    }
    public String getSchoolId() {
        return schoolId;
    }
    public List<Lesson> getLesson() {
        return lesson;
    }



}
