
package nl.hinthr.hogeschoolrotterdamapp.model.RoosterdienstKlaslokalen;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Lesson {

    @SerializedName("LSID")
    @Expose
    private String lSID;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("DayNumber")
    @Expose
    private String dayNumber;
    @SerializedName("PeriodStart")
    @Expose
    private String periodStart;
    @SerializedName("PeriodEnd")
    @Expose
    private String periodEnd;
    @SerializedName("StartTime")
    @Expose
    private String startTime;
    @SerializedName("EndTime")
    @Expose
    private String endTime;
    @SerializedName("Text")
    @Expose
    private String text;
    @SerializedName("StatFlags")
    @Expose
    private Object statFlags;
    @SerializedName("Class")
    @Expose
    private String _class;
    @SerializedName("Subject")
    @Expose
    private String subject;
    @SerializedName("Teacher")
    @Expose
    private String teacher;
    @SerializedName("room")
    @Expose
    private Room room;

    public String getLSID() {
        return lSID;
    }

    public void setLSID(String lSID) {
        this.lSID = lSID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDayNumber() {
        return dayNumber;
    }

    public void setDayNumber(String dayNumber) {
        this.dayNumber = dayNumber;
    }

    public String getPeriodStart() {
        return periodStart;
    }

    public void setPeriodStart(String periodStart) {
        this.periodStart = periodStart;
    }

    public String getPeriodEnd() {
        return periodEnd;
    }

    public void setPeriodEnd(String periodEnd) {
        this.periodEnd = periodEnd;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Object getStatFlags() {
        return statFlags;
    }

    public void setStatFlags(Object statFlags) {
        this.statFlags = statFlags;
    }

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}