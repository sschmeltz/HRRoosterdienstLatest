
package nl.hinthr.hogeschoolrotterdamapp.model.GetPostAppointment;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Appointment {

    @SerializedName("calendar")
    @Expose
    private List<Object> calendar = null;
    @SerializedName("appointments")
    @Expose
    private List<Appointment_> appointments = null;
    @SerializedName("status")
    @Expose
    private String status;

    public List<Object> getCalendar() {
        return calendar;
    }

    public void setCalendar(List<Object> calendar) {
        this.calendar = calendar;
    }

    public List<Appointment_> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment_> appointments) {
        this.appointments = appointments;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
