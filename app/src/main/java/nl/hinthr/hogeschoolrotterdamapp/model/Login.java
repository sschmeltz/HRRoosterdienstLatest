package nl.hinthr.hogeschoolrotterdamapp.model;


public class Login {

    private final boolean isAuthenticated;
    private final String username;
    private final String message;
    private final String key;

    public Login(boolean isAuthenticated, String username, String message, String key) {
        this.isAuthenticated = isAuthenticated;
        this.username = username;
        this.message = message;
        this.key = key;
    }

    public boolean isAuthenticated() { return isAuthenticated; }
    public String getUsername() { return username; }
    public String getMessage() { return message; }
    public String getKey() { return key; }

}
