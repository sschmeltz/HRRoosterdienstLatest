
package nl.hinthr.hogeschoolrotterdamapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClassPOJO {

    @SerializedName("afdeling")

    private String afdeling;
    @SerializedName("capaciteit")

    private int capaciteit;
    @SerializedName("klasnummer")

    private String klasnummer;
    @SerializedName("soortklas")

    private String soortklas;

    public ClassPOJO(String afdeling, int capaciteit, String klasnummer, String soortklas) {
        this.afdeling = afdeling;
        this.capaciteit = capaciteit;
        this.klasnummer = klasnummer;
        this.soortklas = soortklas;
    }


    public String getAfdeling() {
        return afdeling;
    }

    public int getCapaciteit() {
        return capaciteit;
    }

    public String getKlasnummer() {
        return klasnummer;
    }

    public String getSoortklas() {
        return soortklas;
    }



}
