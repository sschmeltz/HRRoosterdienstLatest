
package nl.hinthr.hogeschoolrotterdamapp.model.NewRoosterdienst;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CalendarRooster {

    @SerializedName("calendar")
    @Expose
    private List<Calendar> calendar = null;
    @SerializedName("appointments")
    @Expose
    private List<Object> appointments = null;
    @SerializedName("status")
    @Expose
    private String status;

    public List<Calendar> getCalendar() {
        return calendar;
    }

    public void setCalendar(List<Calendar> calendar) {
        this.calendar = calendar;
    }

    public List<Object> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Object> appointments) {
        this.appointments = appointments;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
