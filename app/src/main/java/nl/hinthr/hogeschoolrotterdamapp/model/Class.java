package nl.hinthr.hogeschoolrotterdamapp.model;

public class Class {

    private String klasnummer;
    private String soortklas;
    private int capaciteit;
    private String afdeling;


    public Class(String klasnummer, String klasnaam, int capaciteit, String afd) {
        this.klasnummer  = klasnummer;
        this.soortklas   = klasnaam;
        this.capaciteit  = capaciteit;
        this.afdeling    = afd;
    }

    //getters
    public int    getCapaciteit() {
        return capaciteit;
    }
    public String getKlasnummer()   { return klasnummer; }
    public String getKlasnaam()     { return soortklas; }
    public String getAfd()          { return afdeling; }


}
