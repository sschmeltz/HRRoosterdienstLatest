package nl.hinthr.hogeschoolrotterdamapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserPageActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.userpage);
        ButterKnife.bind(this);



    }

    static class ViewHolder {
        @BindView(R.id.toolbar)
        Toolbar toolbar;
        @BindView(R.id.toolbar_layout)
        CollapsingToolbarLayout toolbarLayout;
        @BindView(R.id.app_bar)
        AppBarLayout appBar;




        @BindView(R.id.container)
        ConstraintLayout container;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
