package nl.hinthr.hogeschoolrotterdamapp.UserActivities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import nl.hinthr.hogeschoolrotterdamapp.Client.Client;
import nl.hinthr.hogeschoolrotterdamapp.R;
import nl.hinthr.hogeschoolrotterdamapp.model.Class;
import nl.hinthr.hogeschoolrotterdamapp.model.ClassPOJO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class SearchClassroomInformation extends AppCompatActivity {
   public  List<Class> repos;
   public  List<ClassPOJO> repos2;
  // public  List<ClassPOJO> repos;

    private static final String TAG ="SearchClassroom" ;
    @BindView(R.id.spinner1)
    Spinner spinner1;
    @BindView(R.id.klsnummer)
    TextView klsnummer;
    @BindView(R.id.klsnaam)
    TextView klsnaam;
    @BindView(R.id.klscap)
    TextView klscap;
    @BindView(R.id.klstype)
    TextView klstype;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_classroom_information);
        ButterKnife.bind(this);

        Retrofit.Builder builder = new Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .baseUrl("https://api.myjson.com")
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        Client client = retrofit.create(Client.class);
        Call<List<ClassPOJO>> call2 = client.getAllClassesPOJO();
// Execute the call asynchronously. Get a positive or negative callback.
           call2.enqueue(new Callback<List<ClassPOJO>>() {
            @Override
            public void onResponse(@NonNull Call<List<ClassPOJO>> call, @NonNull Response<List<ClassPOJO>> response) {
                repos2 = response.body();
                assert repos2 != null;
                spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        String klaslokaal = adapterView.getItemAtPosition(position).toString();

                        if(repos2!=null) {
                            ArrayList a =  getSpecificClass(repos2, klaslokaal);
                            if(a.size()> 0) {
                                klsnummer.setText(a.get(0).toString());
                                klsnaam.setText(a.get(1).toString());
                                klscap.setText(a.get(2).toString());
                                klstype.setText(a.get(3).toString());
                            }else{
                                klsnummer.setText("");
                                klsnaam.setText("");
                                klscap.setText("");
                                klstype.setText("");
                                Toast.makeText(getApplicationContext(), "no information found..", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(getApplicationContext(), "loading..", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });


            }

            @Override
            public void onFailure(Call<List<ClassPOJO>> call, Throwable t) {

            }
        });





    }

    private ArrayList getSpecificClass(List<ClassPOJO> repos, String klaslokaal) {
        ArrayList<Object> result  =new ArrayList<>();

            for(ClassPOJO item :repos){
                if(klaslokaal.equals(item.getKlasnummer())){
                    result.add(item.getKlasnummer());
                    result.add(item.getSoortklas());
                    result.add(item.getCapaciteit());
                    result.add(item.getAfdeling());
                }
            }



        return result;
    }






}
