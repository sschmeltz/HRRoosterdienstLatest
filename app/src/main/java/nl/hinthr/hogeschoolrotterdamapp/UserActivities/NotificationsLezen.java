package nl.hinthr.hogeschoolrotterdamapp.UserActivities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nl.hinthr.hogeschoolrotterdamapp.Client.NotificationClient;
import nl.hinthr.hogeschoolrotterdamapp.R;
import nl.hinthr.hogeschoolrotterdamapp.SharedPreferencesUtil.Session;
import nl.hinthr.hogeschoolrotterdamapp.UserActivities.Adapters.NotificationsAdapter;
import nl.hinthr.hogeschoolrotterdamapp.model.Notification;
import nl.hinthr.hogeschoolrotterdamapp.model.Notification_;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class NotificationsLezen extends AppCompatActivity {

    @BindView(R.id.notificationRecycleview)
    RecyclerView notificationRecycleview;
    @BindView(R.id.calendarVieww) CalendarView calendarVieww;
    @BindView(R.id.buttonNotificatiesOpzoeken) Button buttonNotificatiesOpzoeken;
    Session session;
    private String             selectedDate;
    private Calendar c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications_lezen);
        ButterKnife.bind(this);
        session = new Session(NotificationsLezen.this);
        notificationRecycleview.setLayoutManager(new LinearLayoutManager(this));
        Context context = notificationRecycleview.getContext();
        int resId2 = R.anim.layout_item_slidefrombottom;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(context, resId2);
        notificationRecycleview.setLayoutAnimation(animation);
        calendarLayoutFunciotnalities();
        //1. SECONDLY, GET SELETED DATE FROM CALENDARVIEW.
        getSelectDateFromCalendarview();

        //THIS FUNCTION WILL CHECK IF THERE IS A DATE SELECTED. IF THERE IS A DATE SELECTED IT THE makeConnectionRetrofitWithUrl() FUNCITON WILL BE ACTIVATED
        KlickButtonToSearchClassroom();
        //1. START WITH MAKING CONNECTION WITH SCHOOL WEBSITE

    }

    private void KlickButtonToSearchClassroom() {
    }

    private void getSelectDateFromCalendarview() {
        calendarVieww.setOnDateChangeListener((CalendarView calendarView, int year, int month, int day) -> {
            Calendar cal  = new GregorianCalendar(year, month, day);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            selectedDate =df.format(cal.getTime());
            Toast.makeText(getApplicationContext(), selectedDate, Toast.LENGTH_SHORT).show();

        });
    }
    private void calendarLayoutFunciotnalities() {
        c = Calendar.getInstance();
        //calendar cant go further than one year
        c.add(Calendar.MONTH, -3);
        calendarVieww.setMinDate(c.getTimeInMillis());
        //Calendar cant go further then 2 months
        c.add(Calendar.MONTH, 3);
        calendarVieww.setMaxDate(c.getTimeInMillis());
        //calendarVieww.setDate(Calendar.MONTH);
    }

    public void makeConnectionRetrofitWithUrl(String selectedDate) {
        Retrofit.Builder builder = new Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .baseUrl("http://145.24.222.157:8080/")
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();


        NotificationClient client = retrofit.create(NotificationClient.class);
        Call<Notification> call3 = client.getNotification( session.getuserkey(),selectedDate,selectedDate);
        // Execute the call asynchronously. Get a positive or negative callback.
        call3.enqueue(new Callback<Notification>() {
            @Override
            public void onResponse(@NonNull Call<Notification> call, @NonNull Response<Notification> response) {
                List<Notification_> res = Objects.requireNonNull(response.body()).getNotifications();
                //Send th data to the Adapter to be inserted
                notificationRecycleview.setAdapter(new NotificationsAdapter(NotificationsLezen.this, res));

            }

            @Override
            public void onFailure(Call<Notification> call, Throwable t) {


            }
        });


    }

    @OnClick(R.id.buttonNotificatiesOpzoeken)
    public void onViewClicked() {
        if ( selectedDate != null){ makeConnectionRetrofitWithUrl(selectedDate);}
        else {                Toast.makeText(getApplicationContext(), "U heeft nog geen datum gekozen. Kies a.u.b een klaslokaal uit", Toast.LENGTH_SHORT).show();
        }
    }
}
