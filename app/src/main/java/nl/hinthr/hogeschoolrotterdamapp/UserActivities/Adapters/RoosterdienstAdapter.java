package nl.hinthr.hogeschoolrotterdamapp.UserActivities.Adapters;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import nl.hinthr.hogeschoolrotterdamapp.R;
import nl.hinthr.hogeschoolrotterdamapp.SharedPreferencesUtil.Session;
import nl.hinthr.hogeschoolrotterdamapp.UserActivities.KlaslokaalRoosterdienst;
import nl.hinthr.hogeschoolrotterdamapp.UserActivities.Reserveren;
import nl.hinthr.hogeschoolrotterdamapp.dialog_login;
import nl.hinthr.hogeschoolrotterdamapp.model.GetPostAppointment.Appointment;
import nl.hinthr.hogeschoolrotterdamapp.model.GetPostAppointment.Appointment_;
import nl.hinthr.hogeschoolrotterdamapp.model.NewRoosterdienst.Calendar;
import nl.hinthr.hogeschoolrotterdamapp.model.NewRoosterdienst.CalendarRooster;
import nl.hinthr.hogeschoolrotterdamapp.model.RoosterdienstKlaslokalen.Lesson;
import nl.hinthr.hogeschoolrotterdamapp.model.RoosterdienstKlaslokalen.RootsterdienstPOJO;

import static nl.hinthr.hogeschoolrotterdamapp.PoppupClass.loggedInCheck;
import static nl.hinthr.hogeschoolrotterdamapp.staticMethodsAndLists.beginLes;
import static nl.hinthr.hogeschoolrotterdamapp.staticMethodsAndLists.eindLes;
import static nl.hinthr.hogeschoolrotterdamapp.staticMethodsAndLists.lesUren;


public class RoosterdienstAdapter extends RecyclerView.Adapter<RoosterdienstAdapter.RoosterdienstViewHOlder>  {
    private Context context;
    private CalendarRooster lessons;
    private final String _selectedDate;
    private  Appointment reservatedAppointment;
    private ArrayList<CalendarListForAdapter> listofHours;
    Session session;
    private String b;
    private String e;


    public RoosterdienstAdapter(KlaslokaalRoosterdienst context, CalendarRooster lessons, String _selectedDate) {
        this.context = context;
        this.lessons = lessons;
        this._selectedDate = _selectedDate;
        MakeCalendarListForAdapter();
        session = new Session(context);
    }

    public RoosterdienstAdapter(KlaslokaalRoosterdienst klaslokaalRoosterdienst, CalendarRooster classroomsResponse, String selectedDate, Appointment response_res) {
        this.context = klaslokaalRoosterdienst;
        this.lessons = classroomsResponse;
        this._selectedDate = selectedDate;
        this.reservatedAppointment = response_res;
        MakeCalendarListForAdapter();
        session = new Session(context);
    }

    public interface onItemClickListener{

    }
    private void MakeCalendarListForAdapter() {
        List<Calendar> les = lessons.getCalendar();
        List<Appointment_> reservatedLes = reservatedAppointment.getAppointments();

        ArrayList<CalendarListForAdapter> calendarListForAdapters = new ArrayList<>();
        int bound = beginLes.size();
        /*1.This while loop will make 3 lists out of one. lesUren has all the lesson hour numbers ,
         beginLes has the start lessons and eindLes the end of each lesson. by the help of a model*/
        int i = 0;
        if (i < bound)
        { do { CalendarListForAdapter calendarListForAdapter =
                        new CalendarListForAdapter(lesUren.get(i), beginLes.get(i), eindLes.get(i));
                calendarListForAdapters.add(calendarListForAdapter);
                i++;
            } while (i < bound);
        }
        /*2.when the while loop above is finished and has created a new list with all the lesson hours,it willgo in a new for loop to filter out
        * the hours where there is a reservated lesson. for this it will use a list with all the reservated lesson from the api url link*/

        // 3.check if the are reservated lessons to filter out.some days there are no lessons. for example the weekends or weeks of vacation.
        if (les != null) {

            for (Calendar le : les) {
                int p = Integer.parseInt(le.getPeriodStart());
                int e = Integer.parseInt(le.getPeriodEnd());
                calendarListForAdapters.removeIf(c -> Integer.parseInt(c.getUur_()) >= p && Integer.parseInt(c.getUur_()) <= e);
            }
        }

        if (reservatedLes != null) {

            for (Appointment_ less : reservatedLes) {
                String p = less.getStartTime();
                String e = less.getEndTime();
                calendarListForAdapters.removeIf(c -> c.getBegintTijd_().equals(p) && c.getEindTijd_().equals(e));
            }
        }

        /*4.when all of the above is finished, the list is now ready for the adapter to be used */
        listofHours = calendarListForAdapters;
    }


    @NonNull
    @Override
    public RoosterdienstViewHOlder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.klasroom_item, parent, false);
        return new RoosterdienstViewHOlder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RoosterdienstViewHOlder holder, int position) {

           CalendarListForAdapter leshour = listofHours.get(position);
           /*check if the list is not null */
           if(leshour!=null) {
               /*If data in the list has no null values, the data will be put in every position in the recycleview */
               if (leshour.getUur_()        != null) { holder.lestijd.setText(leshour.getUur_());             }
               if (leshour.getBegintTijd_() != null) { holder.beginlestijd.setText(leshour.getBegintTijd_()); }
               if (leshour.getTeacher()     != null) { holder.eindlestijd.setText(leshour.getEindTijd_());    } }
            //holder aangetikt? Als de gebruiker is ingelogd kan hij reserveren, ander moet hij inloggen.
           holder.cardviewklasroom.setOnClickListener(view -> Reserveren(holder));
    }




    private void Reserveren(RoosterdienstViewHOlder holder) {
        b = holder.beginlestijd.getText().toString();
        e = holder.eindlestijd.getText().toString();
        session.setbegintijd(holder.beginlestijd.getText().toString());
        session.seteindtijd(holder.eindlestijd.getText().toString());
        if (IsUserLoggedin() == true) {//check als de gebruiker is ingelogd.
            //gebruiker is niet ingelogd, gebruiker moet inloggen om te reserven
            Intent intent = new Intent(context, Reserveren.class);
            intent.putExtra("beginlestijd", b);
            intent.putExtra("eindlestijd",  e);
            intent.putExtra("selectedDate",  _selectedDate);
            context.startActivity(intent);

        } else {
            //   Toast.makeText(context, "Wilt u tussen " + holder.beginlestijd.getText() + "-" + holder.eindlestijd.getText() + " uur een reservering plaatsen?", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(context, dialog_login.class);
            context.startActivity(intent);
        }
    }


    private boolean IsUserLoggedin() {
        if ( session.getusename().length() > 1 ||  session.getuserkey().length() > 1) {
            return true;
        } else {
            return false;
        }
    }
    private void changeTheTextColor_And_BoxColor_Of_The_RecycleviewBox(RoosterdienstViewHOlder holder) {
        holder.lineartimebackground.setBackground(ContextCompat.getDrawable(context, R.drawable.colorgradebabyblue));
        holder.linearlili.setBackground(ContextCompat.getDrawable(context, R.drawable.colorgradered));
        holder.naam.setTextColor(ContextCompat.getColor(context, R.color.white));//CHANGE THE TEXT COLOR DOCENT.
        holder.subject.setTextColor(ContextCompat.getColor(context, R.color.white));//CHANGE THE TEXT COLOR subject.
        holder.text_.setTextColor(ContextCompat.getColor(context, R.color.white));//CHANGE THE TEXT COLOR text.
        holder.klassen.setTextColor(ContextCompat.getColor(context, R.color.white));//CHANGE THE TEXT COLOR klassen.
        holder.klassen.setTextColor(ContextCompat.getColor(context, R.color.white));//CHANGE THE TEXT COLOR klassen.

    }




@Override
public long getItemId(int position) {
        return position;
        }

    @Override
    public int getItemViewType(int position) {
        return position;

    }
    @Override
    public int getItemCount() {
        return listofHours.size();
    }



    public class RoosterdienstViewHOlder extends RecyclerView.ViewHolder {
        private RecyclerView listofklaslokaalitems;
        public CardView cardviewklasroom;
        public TextView klassen;
        public TextView naam;
        public TextView eindeLes;
        public TextView lestijd;
        public TextView subject;
        public TextView beginlestijd;
        public TextView eindlestijd;
        public TextView text_;
        public TextView texttijden;
        public LinearLayout lineartimebackground;
        public LinearLayout linearlili;

        RoosterdienstViewHOlder(View itemView) {
            super(itemView);
            naam = itemView.findViewById(R.id.naam);
            lestijd = itemView.findViewById(R.id.lestijd);
            subject = itemView.findViewById(R.id.subject);

            text_ = itemView.findViewById(R.id.text_);
            beginlestijd = itemView.findViewById(R.id.beginlestijd);
            eindlestijd = itemView.findViewById(R.id.eindlestijd);
            cardviewklasroom = itemView.findViewById(R.id.cardviewklasroom);
            lineartimebackground = itemView.findViewById(R.id.lineartimebackground);
            linearlili = itemView.findViewById(R.id.linearlili);

            listofklaslokaalitems = itemView.findViewById(R.id.listofklaslokaalitems);


        }
    }


}

