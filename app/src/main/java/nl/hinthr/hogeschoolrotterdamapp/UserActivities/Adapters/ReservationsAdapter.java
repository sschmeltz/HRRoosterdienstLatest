package nl.hinthr.hogeschoolrotterdamapp.UserActivities.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import nl.hinthr.hogeschoolrotterdamapp.Client.AppointmentAPI;
import nl.hinthr.hogeschoolrotterdamapp.Client.newRoosterdienstApi;
import nl.hinthr.hogeschoolrotterdamapp.R;
import nl.hinthr.hogeschoolrotterdamapp.SharedPreferencesUtil.Session;
import nl.hinthr.hogeschoolrotterdamapp.UserActivities.KlaslokaalRoosterdienst;
import nl.hinthr.hogeschoolrotterdamapp.UserActivities.MyReservationsClass;
import nl.hinthr.hogeschoolrotterdamapp.model.GetPostAppointment.Appointment;
import nl.hinthr.hogeschoolrotterdamapp.model.GetPostAppointment.Appointment_;
import nl.hinthr.hogeschoolrotterdamapp.model.NewRoosterdienst.CalendarRooster;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ReservationsAdapter extends RecyclerView.Adapter<ReservationsAdapter.ReservationsViewHolder>  {
    private Appointment listOfReservations;
    private Context context;
    private View view;
    Session session;

    public ReservationsAdapter(MyReservationsClass context, Appointment listOfNotifications) {

        this.context = context;
        this.listOfReservations = listOfNotifications;
        session = new Session(context);
    }



    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;

    }

    @NonNull
    @Override
    public ReservationsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder holder ;
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.myreservations_item, parent, false);

        return new ReservationsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReservationsViewHolder holder, int position) {
        if(listOfReservations!=null)
        {
            // if the values are not null put the data in the UI
            Appointment_ list = listOfReservations.getAppointments().get(position);
            if(list.getCreator()     !=null)holder.idemail.setText(list.getCreator());
            if(list.getSubjectCode() !=null)holder.idklascode.setText(list.getSubjectCode());
            if(list.getRoom()        !=null)holder.idklaslokaal.setText(list.getRoom());
            if(list.getClassName()   !=null)holder.idklas.setText(list.getClassName());
            if(list.getDate()        !=null)holder.datum.setText(list.getDate());
            if(list.getStartTime()   !=null)holder.idbegin.setText(list.getStartTime());
            if(list.getEndTime()     !=null)holder.ideind.setText(list.getEndTime());
       //     if(list.getImage()   !=null)holder.notificationImage.setImageBitmap(list.getImage());


            view.setOnLongClickListener(view -> {
                DeleteReservation(list.getId());
                Toast.makeText(context,"Item at position "+position+" deleted",Toast.LENGTH_SHORT).show();
                listOfReservations.getAppointments().remove(position);
                notifyDataSetChanged();
                return true;
            });


        }
    }

    @Override
    public int getItemCount() {
        if(listOfReservations.getAppointments()!=null){
        return listOfReservations.getAppointments().size();}else{
            return 0;
        }
    }
   ///Delete a reservation
    public void DeleteReservation( int id) {
        Retrofit.Builder builder = new Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .baseUrl("http://145.24.222.157:8080/")
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        AppointmentAPI client = retrofit.create(AppointmentAPI.class);
        ///send to the client  id of the reservation to delete
        Call<Appointment> call3 = client.DeleteAReservation(session.getuserkey(),id);
        call3.enqueue(new Callback<Appointment>() {
            @Override
            public void onResponse(Call<Appointment> call, Response<Appointment> response) {

            }
            @Override
            public void onFailure(Call<Appointment> call, Throwable t) {
            }
        });


    }

    public class ReservationsViewHolder extends RecyclerView.ViewHolder{
        public TextView idklascode;
        public TextView idklaslokaal;
        public TextView idklas;
        public TextView datum;
        public TextView idbegin;
        public TextView ideind;
        public TextView idemail;

        public ReservationsViewHolder(View itemView) {
            super(itemView);
            idklascode   = itemView.findViewById(R.id.idklascode);
            idklaslokaal = itemView.findViewById(R.id.idklaslokaal);
            idklas       = itemView.findViewById(R.id.idklas);
            datum        = itemView.findViewById(R.id.datum);
            idbegin      = itemView.findViewById(R.id.idbegin);
            ideind       = itemView.findViewById(R.id.ideind);
            idemail      = itemView.findViewById(R.id.idemail);
        }
    }
}
