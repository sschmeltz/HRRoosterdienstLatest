package nl.hinthr.hogeschoolrotterdamapp.UserActivities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nl.hinthr.hogeschoolrotterdamapp.Client.AppointmentAPI;
import nl.hinthr.hogeschoolrotterdamapp.R;
import nl.hinthr.hogeschoolrotterdamapp.SharedPreferencesUtil.Session;
import nl.hinthr.hogeschoolrotterdamapp.UserActivities.Adapters.ReservationsAdapter;
import nl.hinthr.hogeschoolrotterdamapp.model.GetPostAppointment.Appointment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by Gebruiker on 9-3-2018.
 */

public class MyReservationsClass extends AppCompatActivity {
    @BindView(R.id.calendarViewwMyreservations)
    CalendarView calendarViewwMyreservations;
    @BindView(R.id.buttonMyreservations)
    Button buttonMyreservations;
    @BindView(R.id.myReservationsRecycleview)
    RecyclerView myReservationsRecycleview;
    @BindView(R.id.notificationTitle)
    TextView notificationTitle;
    private Session session;

    private Calendar c;
    private String selectedDate;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_reservations);
        ButterKnife.bind(this);
        session = new Session(MyReservationsClass.this);
        myReservationsRecycleview.setLayoutManager(new LinearLayoutManager(this));
        Context context = myReservationsRecycleview.getContext();
        int resId2 = R.anim.layout_item_slidefrombottom;
        calendarLayoutFunciotnalities();
        getSelectDateFromCalendarview();
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(context, resId2);
        myReservationsRecycleview.setLayoutAnimation(animation);
    }
    private void getSelectDateFromCalendarview() {
        calendarViewwMyreservations.setOnDateChangeListener((CalendarView calendarView, int year, int month, int day) -> {
            Calendar cal  = new GregorianCalendar(year, month, day);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            selectedDate =df.format(cal.getTime());
            Toast.makeText(getApplicationContext(), selectedDate, Toast.LENGTH_SHORT).show();

        });
    }


    private void calendarLayoutFunciotnalities() {
        c = Calendar.getInstance();

    }

    @OnClick(R.id.buttonMyreservations)
    public void onViewClicked() {
        if ( selectedDate != null)
        {
            makeConnectionRetrofitWithUrl(selectedDate);}
        else
            {
            Toast.makeText(getApplicationContext(),
                "U heeft nog geen datum gekozen. Kies a.u.b een klaslokaal uit",
                Toast.LENGTH_SHORT).show();
            }
    }

    public void makeConnectionRetrofitWithUrl(String selectedDate) {
        Retrofit.Builder builder = new Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .baseUrl("http://145.24.222.157:8080/")
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();


        AppointmentAPI client = retrofit.create(AppointmentAPI.class);
        Call<Appointment> call3 = client.GetAReservation(session.getuserkey(),selectedDate,selectedDate);
        // Execute the call asynchronously. Get a positive or negative callback.
        call3.enqueue(new Callback<Appointment>() {
            @Override
            public void onResponse(Call<Appointment> call, Response<Appointment> response) {
                Appointment response_res = response.body();
                //Send th data to the Adapter to be inserted
                myReservationsRecycleview.setAdapter(new ReservationsAdapter(MyReservationsClass.this, response_res));
            }

            @Override
            public void onFailure(Call<Appointment> call, Throwable t) {

            }
        });

    }
}
