package nl.hinthr.hogeschoolrotterdamapp.UserActivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nl.hinthr.hogeschoolrotterdamapp.Client.AppointmentAPI;
import nl.hinthr.hogeschoolrotterdamapp.R;
import nl.hinthr.hogeschoolrotterdamapp.SharedPreferencesUtil.Session;
import nl.hinthr.hogeschoolrotterdamapp.model.GetPostAppointment.Appointment;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static nl.hinthr.hogeschoolrotterdamapp.UserActivities.KlaslokaalRoosterdienst.selectedDate;

public class Reserveren extends AppCompatActivity {

    @BindView(R.id.wiltureserveren)
    TextView wiltureserveren;


    @BindView(R.id.idOnderwerp)
    AutoCompleteTextView idOnderwerp;
    @BindView(R.id.idLescode)
    AutoCompleteTextView idLescode;
    @BindView(R.id.reserven2)
    Button reserven2;
    @BindView(R.id.annuleren2)
    Button annuleren2;
    @BindView(R.id.idklaslokaal)
    Spinner idklaslokaal;
    @BindView(R.id.idklassen)
    Spinner idklassen;
    @BindView(R.id.linearLayout1)
    LinearLayout linearLayout1;
   // private String selectedDate;
    private Session session;
    private String beginLes;
    private String eindLes;
    private String onderwep;
    private String lescode;
    String selectedClassroomFromSpinner;
    private String selectedClassFromSpinner;
    private String userkey;
    private String selecteddate;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserveren);
        ButterKnife.bind(this);
         intent = getIntent();
        session = new Session(Reserveren.this);

        beginLes      = intent.getStringExtra("beginlestijd");
        eindLes       = intent.getStringExtra("eindlestijd");
        wiltureserveren.setText(String.format("Wilt u tussen %s-%s uur een reservering plaatsen?", beginLes, eindLes));


        idklaslokaal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                selectedClassroomFromSpinner = adapterView.getItemAtPosition(position).toString();
                }
                @Override
                public void onNothingSelected(AdapterView<?> adapterView) { }
                });



        idklassen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                selectedClassFromSpinner = adapterView.getItemAtPosition(position).toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });



    }

    @OnClick(R.id.reserven2)
    public void onReserven2Clicked() {
        userkey       = session.getuserkey();
        selecteddate  = selectedDate;
        onderwep      = idOnderwerp.getText().toString();
        lescode       = idLescode.getText().toString();
        Retrofit builder = new Retrofit.Builder()
                .baseUrl("http://145.24.222.157:8080/")
                .client(new OkHttpClient())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        AppointmentAPI appointment = builder.create(AppointmentAPI.class);

        Call<Appointment> call = appointment.PostAReservation(
                userkey,
                onderwep.trim(),
                lescode.trim(),
                selectedClassFromSpinner.trim(),
                selectedClassroomFromSpinner.trim(),
                selecteddate.trim(),
                beginLes.trim(),
                eindLes.trim());

      call.enqueue(new Callback<Appointment>() {
            @Override
            public void onResponse(Call<Appointment> call, Response<Appointment> response) {
                if(call.isExecuted()){
                    assert response.body() != null;
                    response.body();
                    finish();

                }
            }

            @Override
            public void onFailure(Call<Appointment> call, Throwable t) {

            }
        });




    }

    @OnClick(R.id.annuleren2)
    public void onAnnuleren2Clicked() {
        finish();
    }
}
