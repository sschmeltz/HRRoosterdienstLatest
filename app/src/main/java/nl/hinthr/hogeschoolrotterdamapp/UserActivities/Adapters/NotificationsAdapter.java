package nl.hinthr.hogeschoolrotterdamapp.UserActivities.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import nl.hinthr.hogeschoolrotterdamapp.R;
import nl.hinthr.hogeschoolrotterdamapp.UserActivities.NotificationsLezen;
import nl.hinthr.hogeschoolrotterdamapp.model.Notification_;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.NotificationsViewHolder>  {
    private  List<Notification_> listOfNotifications;
    private Context context;

    public NotificationsAdapter(NotificationsLezen context, List<Notification_> listOfNotifications) {

        this.context = context;
        this.listOfNotifications = listOfNotifications;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;

    }

    @NonNull
    @Override
    public NotificationsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.notification_item, parent, false);
        return new NotificationsAdapter.NotificationsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationsViewHolder holder, int position) {
        if(listOfNotifications!=null)
        {
            Notification_ list = listOfNotifications.get(position);
            if(list.getCategory()   !=null)holder.notificationCategorie.setText(list.getCategory());
            if(list.getDescription()!=null)holder.notificationDescription.setText(list.getDescription());
            if(list.getLocation()   !=null)holder.notificationLocation.setText(list.getLocation());
        }
    }

    @Override
    public int getItemCount() {
        return listOfNotifications.size();
    }

    public class NotificationsViewHolder extends RecyclerView.ViewHolder{


        public   ImageView notificationImage;
        public   TextView notificationCategorie;
        public   TextView notificationDescription;
        public   TextView notificationLocation;

        public NotificationsViewHolder(View itemView) {
            super(itemView);

            notificationImage       = itemView.findViewById(R.id.notificationImage);
            notificationCategorie   = itemView.findViewById(R.id.notificationCategorie);
            notificationDescription = itemView.findViewById(R.id.notificationDescription);
            notificationLocation    = itemView.findViewById(R.id.notificationLocation);
        }
    }
}
