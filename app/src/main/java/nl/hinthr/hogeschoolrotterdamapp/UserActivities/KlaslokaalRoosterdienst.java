package nl.hinthr.hogeschoolrotterdamapp.UserActivities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import nl.hinthr.hogeschoolrotterdamapp.Client.AppointmentAPI;
import nl.hinthr.hogeschoolrotterdamapp.Client.RoosterdienstApi;

import nl.hinthr.hogeschoolrotterdamapp.Client.newRoosterdienstApi;
import nl.hinthr.hogeschoolrotterdamapp.R;
import nl.hinthr.hogeschoolrotterdamapp.SharedPreferencesUtil.Session;
import nl.hinthr.hogeschoolrotterdamapp.UserActivities.Adapters.ReservationsAdapter;
import nl.hinthr.hogeschoolrotterdamapp.UserActivities.Adapters.RoosterdienstAdapter;
import nl.hinthr.hogeschoolrotterdamapp.model.GetPostAppointment.Appointment;
import nl.hinthr.hogeschoolrotterdamapp.model.NewRoosterdienst.CalendarRooster;
import nl.hinthr.hogeschoolrotterdamapp.model.RoosterdienstKlaslokalen.RootsterdienstPOJO;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class KlaslokaalRoosterdienst extends AppCompatActivity {
    @BindView(R.id.spinnerRoosterdienst)      Spinner spinnerRoosterdienst;
    @BindView(R.id.calendarVieww)             CalendarView calendarVieww;
    @BindView(R.id.textGrid2)                 TextView textGrid2;
    @BindView(R.id.buttonKlaslokaaloOpzoeken) Button buttonKlaslokaaloOpzoeken;
    @BindView(R.id.listofklaslokaalitems)     RecyclerView listofklaslokaalitems;
    @BindView(R.id.setlistviewheight)         LinearLayout setlistviewheight;
    private RootsterdienstPOJO reposClassroom;
    private Context            context;
    private Calendar           c;
    private String             klaslokaal;
    public static String             selectedDate;
    private RoosterdienstApi   roosterdienstApi;
    private newRoosterdienstApi newRoosterdienstApi;
    private nl.hinthr.hogeschoolrotterdamapp.model.NewRoosterdienst.Calendar newRoosterdienstApi2;
    int resId;
    Session session;
    private CalendarRooster classroomsResponse;
    private Appointment response_res;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_klaslokaal_roosterdienst);
        ButterKnife.bind(this);
        listofklaslokaalitems.setLayoutManager(new LinearLayoutManager(this));
        context = listofklaslokaalitems.getContext();
        resId = R.anim.layout_item_slidefrombottom;
        session = new Session(KlaslokaalRoosterdienst.this);

        //1. CALENDAR LAYOUT FUNCTIONALITIES.
        calendarLayoutFunciotnalities();

       // GetKlasroomHours();

        //2. FIRSTLY, GET SELECTED CLASROOM FROM SPINNER.
        getSelectClassroomFromSpinner();

        //3. SECONDLY, GET SELETED DATE FROM CALENDARVIEW.
        getSelectDateFromCalendarview();

        //4. IF CLASROOM AND DATE ARE SELECTED,  I WILL GO TO THE FUNCTION NAMED 'GetKlasroomApiData()'.
        // IT WILL USE GET REQUEST FUNCTION THAT TAKES THE
        // CHOSEN CLASROON EN DATE AS PARAMETERS TO GET THE DATA FROM THE API AS JSON.
       // runAnimation(listofklaslokaalitems);
        KlickButtonToSearchClassroom();

    }

    private void calendarLayoutFunciotnalities() {
        c = Calendar.getInstance();
        //calendar cant go further than one year
        c.add(Calendar.YEAR, 0);
        calendarVieww.setMinDate(c.getTimeInMillis());
        //Calendar cant go further then 2 months
        c.add(Calendar.MONTH, 2);
        calendarVieww.setMaxDate(c.getTimeInMillis());
        calendarVieww.setDate(Calendar.MONTH);
    }

    private void getSelectClassroomFromSpinner() {
        spinnerRoosterdienst.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                klaslokaal = adapterView.getItemAtPosition(position).toString();
                Toast.makeText(getApplicationContext(), klaslokaal, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });
    }

    private void getSelectDateFromCalendarview() {
        calendarVieww.setOnDateChangeListener((CalendarView calendarView, int year, int month, int day) -> {
            Calendar cal  = new GregorianCalendar(year, month, day);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            selectedDate =df.format(cal.getTime());
            Toast.makeText(getApplicationContext(), selectedDate, Toast.LENGTH_SHORT).show();

        });

    }

    private void KlickButtonToSearchClassroom() {
        buttonKlaslokaaloOpzoeken.setOnClickListener(view -> {
            if (klaslokaal != null & selectedDate == null) {
                Toast.makeText(
                        getApplicationContext(),
                        "U heeft nog geen klaslokaal gekozen. Kies a.u.b een klaslokaal uit",
                        Toast.LENGTH_SHORT).show();
            }
            else if (klaslokaal == null & selectedDate != null) {
                Toast.makeText(
                        getApplicationContext(),
                        "U heeft nog geen datum gekozen. Kies a.u.b een datum uit.",
                        Toast.LENGTH_SHORT).show();
            }
            else if (klaslokaal == null & selectedDate == null) {
                Toast.makeText(getApplicationContext(),
                        "U heeft nog geen datum gekozen. Kies a.u.b een datum uit",
                        Toast.LENGTH_SHORT).show();
            } else {

                LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(context, resId);
                listofklaslokaalitems.setLayoutAnimation(animation);
                //GetKlasroomApiData(klaslokaal, selectedDate);
                GetKlasroomApiData2(klaslokaal, selectedDate);

            }

        });
    }

    public void GetKlasroomApiData2(String _klaslokaal, String _selectedDate) {

        Retrofit.Builder builder = new Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .baseUrl("http://145.24.222.157:8080/")
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();


        AppointmentAPI client = retrofit.create(AppointmentAPI.class);
        Call<Appointment> call3 = client.GetAReservation(session.getuserkey(),selectedDate,selectedDate);
        // Execute the call asynchronously. Get a positive or negative callback.
        call3.enqueue(new Callback<Appointment>() {
            @Override
            public void onResponse(Call<Appointment> call, Response<Appointment> response) {
                 response_res = response.body();
                //Send th data to the Adapter to be inserted

            }

            @Override
            public void onFailure(Call<Appointment> call, Throwable t) {

            }
        });

        Retrofit builder2 = new Retrofit.Builder()
                .baseUrl("http://145.24.222.157:8080")
                .client(new OkHttpClient())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        newRoosterdienstApi = builder2.create(newRoosterdienstApi.class);
        Call<CalendarRooster> call = newRoosterdienstApi.getClassroom(
                _klaslokaal,
                _selectedDate,
                _selectedDate);
        // Execute the call asynchronously. Get a positive or negative callback.
        call.enqueue(new Callback<CalendarRooster>() {
            @Override
            public void onResponse(@NonNull Call<CalendarRooster> call,
                                   @NonNull Response<CalendarRooster> response) {


              classroomsResponse=  response.body();
            //Send the data to the Adapter to be inserted
                if(response_res==null) {
                    listofklaslokaalitems.setAdapter(
                            new RoosterdienstAdapter(KlaslokaalRoosterdienst.this, classroomsResponse, _selectedDate));
                }else {
                    listofklaslokaalitems.setAdapter( new RoosterdienstAdapter(KlaslokaalRoosterdienst.this, classroomsResponse, _selectedDate, response_res));

                }

            }

            @Override
            public void onFailure(Call<CalendarRooster> call, Throwable t) {

            }
        });
    }





}
