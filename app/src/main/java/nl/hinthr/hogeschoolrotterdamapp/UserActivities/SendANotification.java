package nl.hinthr.hogeschoolrotterdamapp.UserActivities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nl.hinthr.hogeschoolrotterdamapp.Client.NotificationClient;
import nl.hinthr.hogeschoolrotterdamapp.R;
import nl.hinthr.hogeschoolrotterdamapp.SharedPreferencesUtil.Session;
import nl.hinthr.hogeschoolrotterdamapp.model.Notification;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by Gebruiker on 27-3-2018.
 */

public class SendANotification extends AppCompatActivity {

    @BindView(R.id.fotomakenCardview)
    CardView fotomakenCardview;
    @BindView(R.id.omschrijvingCardview)
    CardView omschrijvingCardview;
    @BindView(R.id.klaslokaalKiezenCardview) Spinner klaslokaalKiezenCardview;
    @BindView(R.id.CategorieCardview)        Spinner CategorieCardview;
    @BindView(R.id.gemaaktefoto)            ImageView gemaaktefoto;
    @BindView(R.id.mytextText)              EditText mytproblemText;
    @BindView(R.id.sendNotification) Button sendNotification;

    private String klaslokaalspinner;
    private String categoriespinner;
    private Bitmap bitmap;

    Session session;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_pagina);
        ButterKnife.bind(this);
        klaslokaalKiezenCardview();
        categorieKiezenCardview();
        session = new Session(SendANotification.this); //in oncreate

    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        bitmap = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
        gemaaktefoto.setImageBitmap(bitmap);


    }


    @OnClick(R.id.fotomakenCardview)
    public void onFotomakenCardviewClicked() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 0);
    }

    @OnClick(R.id.sendNotification)
    public void onSendNotificationClicked() {
    }

    @OnClick(R.id.fotomakenCardview)
    public void onfotomakenCardviewViewClicked() {
        // Code here executes on main thread after user presses button
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 0);
    }

    public void klaslokaalKiezenCardview() {
        klaslokaalKiezenCardview.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                klaslokaalspinner = adapterView.getItemAtPosition(position).toString();
                Toast.makeText(getApplicationContext(), klaslokaalspinner, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }
    public void categorieKiezenCardview() {
        CategorieCardview.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                categoriespinner = adapterView.getItemAtPosition(position).toString();
                Toast.makeText(getApplicationContext(), klaslokaalspinner, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }



    private String imageToString(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte[] imgByte = byteArrayOutputStream.toByteArray();
        return android.util.Base64.encodeToString(imgByte, android.util.Base64.DEFAULT);

    }

    @OnClick(R.id.sendNotification)
    public void onViewClicked() {
        String textProblem = mytproblemText.getText().toString();
        if (gemaaktefoto != null || klaslokaalspinner != null || session.getusename() != null || session.getuserkey() != null) {
            Toast.makeText(getApplicationContext(), gemaaktefoto + "\n " + klaslokaalspinner + "\n " + categoriespinner + " \n" + textProblem, Toast.LENGTH_SHORT).show();

            Retrofit.Builder builder = new Retrofit.Builder()
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .baseUrl("http://145.24.222.157:8080/")
                    .addConverterFactory(GsonConverterFactory.create());
            Retrofit retrofit = builder.build();

            NotificationClient client = retrofit.create(NotificationClient.class);
            Call<Notification> call2 = client.postNotification(
                    session.getuserkey(),
                    session.getusename(),
                    "picca", categoriespinner,
                    mytproblemText.getText().toString().trim(), klaslokaalspinner);
            call2.enqueue(new Callback<Notification>() {
                @Override
                public void onResponse(Call<Notification> call, Response<Notification> response) {



                }

                @Override
                public void onFailure(Call<Notification> call, Throwable t) {

                }
            });

        }else{
            Toast.makeText(getApplicationContext(),  "Kies minstends een klaslokaal uit en beschrijf het probleem", Toast.LENGTH_SHORT).show();

        }
    }
}



