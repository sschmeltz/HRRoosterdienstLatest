package nl.hinthr.hogeschoolrotterdamapp.UserActivities.Adapters;

class CalendarListForAdapter {
    private  String eindTijd_;
    private  String teacher;
    private  String subject;
    private  String teacher_;
    private  String subject_;
    private  String text_;
    private  String uur_;
    private  String begintTijd_;


    public void setEindTijd_(String eindTijd_) {
        this.eindTijd_ = eindTijd_;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTeacher_() {
        return teacher_;
    }

    public void setTeacher_(String teacher_) {
        this.teacher_ = teacher_;
    }

    public String getSubject_() {
        return subject_;
    }

    public void setSubject_(String subject_) {
        this.subject_ = subject_;
    }

    public String getText_() {
        return text_;
    }

    public void setText_(String text_) {
        this.text_ = text_;
    }

    public void setUur_(String uur_) {
        this.uur_ = uur_;
    }

    public void setBegintTijd_(String begintTijd_) {
        this.begintTijd_ = begintTijd_;
    }

    public String getUur_() {
        return uur_;
    }

    public String getBegintTijd_() {
        return begintTijd_;
    }

    public String getEindTijd_() {
        return eindTijd_;
    }




    public CalendarListForAdapter(String Uur_, String begintTijd_, String eindTijd_, String subject, String text_) {
        this(Uur_, begintTijd_, eindTijd_, "", subject, text_);
    }


    public CalendarListForAdapter(String Uur_, String begintTijd_, String eindTijd_) {
        this(Uur_, begintTijd_, eindTijd_, "","" ,"" );
    }


    public CalendarListForAdapter(String Uur_, String begintTijd_, String eindTijd_,String teacher, String subject, String text_){

        uur_ = Uur_;
        this.begintTijd_ = begintTijd_;
        this.eindTijd_ = eindTijd_;
        this.teacher = teacher;
        this.subject = subject;
        this.text_ = text_;
    }
}
