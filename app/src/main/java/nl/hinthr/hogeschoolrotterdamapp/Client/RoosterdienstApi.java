package nl.hinthr.hogeschoolrotterdamapp.Client;

import nl.hinthr.hogeschoolrotterdamapp.model.RoosterdienstKlaslokalen.RootsterdienstPOJO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RoosterdienstApi {
    String Key = "xsp/public/InfApp/2018gr5/getLokaalRooster.xsp?sharedSecret=0904181-6otYcvUpCPTe7Jze69qiMm7HbEaR62niXez&";




    @GET(Key+"&json")
    Call<RootsterdienstPOJO> getClassroom(
            @Query("element") String klaslokaal, @Query("startDate") String start, @Query("endDate") String end);
}
