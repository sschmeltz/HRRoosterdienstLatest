package nl.hinthr.hogeschoolrotterdamapp.Client;

import java.util.List;

import nl.hinthr.hogeschoolrotterdamapp.model.Class;
import nl.hinthr.hogeschoolrotterdamapp.model.ClassPOJO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Client {


    @GET("/bins/9l3ue/")
    Call<List<Class>> getAllClasses();
    //put the data in the classpojo
    @POST("/notification")
    Call<List<ClassPOJO>> sendNotification();

    //put the data in the classpojo
    @GET("/bins/9l3ue/")
    Call<List<ClassPOJO>> getAllClassesPOJO();


}
//https://hint.hr.nl/xsp/public/InfApp/2018gr5/getLokaalRooster.xsp?
// sharedSecret=0904181-6otYcvUpCPTe7Jze69qiMm7HbEaR62niXez&element=WD.01.016
                                                     // &startDate =2018-05-01
                                                     // &endDate   =2018-05-31