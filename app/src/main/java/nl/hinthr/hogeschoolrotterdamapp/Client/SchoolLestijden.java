package nl.hinthr.hogeschoolrotterdamapp.Client;
import java.util.List;

import nl.hinthr.hogeschoolrotterdamapp.model.RoosterdienstKlaslokalen.SchoolHour;
import retrofit2.Call;
import retrofit2.http.GET;

public interface SchoolLestijden {

    //api.myjson.com/bins/60xq2

    @GET("/bins/y8vsq")
    Call<List<SchoolHour>> getSchoolClassHours();
}
