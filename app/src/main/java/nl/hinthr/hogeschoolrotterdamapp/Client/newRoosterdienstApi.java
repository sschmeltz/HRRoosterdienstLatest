package nl.hinthr.hogeschoolrotterdamapp.Client;

import java.util.List;

import nl.hinthr.hogeschoolrotterdamapp.model.NewRoosterdienst.CalendarRooster;
import nl.hinthr.hogeschoolrotterdamapp.model.RoosterdienstKlaslokalen.RootsterdienstPOJO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface newRoosterdienstApi {

    //put the data in the  model CalendarRooster
    @GET("/calendar/{room}/")
    Call<CalendarRooster> getClassroom(
                                       @Path("room"  ) String room,
                                       @Query("startDate") String start,
                                       @Query("endDate"  ) String end);





}



