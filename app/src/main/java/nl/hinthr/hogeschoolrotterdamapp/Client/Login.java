package nl.hinthr.hogeschoolrotterdamapp.Client;

import java.util.List;

import nl.hinthr.hogeschoolrotterdamapp.model.Class;
import nl.hinthr.hogeschoolrotterdamapp.model.RoosterdienstKlaslokalen.RootsterdienstPOJO;
import nl.hinthr.hogeschoolrotterdamapp.model.User;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface  Login {


    @GET("login")
     Call<User> getLogin(@Query("username") String username, @Query("password") String password);
}

