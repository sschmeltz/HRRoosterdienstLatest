package nl.hinthr.hogeschoolrotterdamapp.Client;

import nl.hinthr.hogeschoolrotterdamapp.model.GetPostAppointment.Appointment;
import nl.hinthr.hogeschoolrotterdamapp.model.NewRoosterdienst.CalendarRooster;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface AppointmentAPI {

    @POST("calendar/")
    Call<Appointment> PostAReservation(
            @Query("authKey"    ) String authKey,
            @Query("subjectName") String subjectName,
            @Query("subjectCode") String subjectCode,
            @Query("room"       ) String room,
            @Query("className"  ) String className,
            @Query("date"       ) String date,
            @Query("startTime"  ) String startTime,
            @Query("endTime"    ) String endTime);

    @GET("calendar/")
    Call<Appointment> GetAReservation(
            @Query("authKey"    ) String authKey,
            @Query("startDate"  ) String startDate,
            @Query("endDate"    ) String endDate);

   @DELETE("calendar/")
    Call<Appointment> DeleteAReservation(
            @Query("authKey") String authKey,
            @Query("id"     )        int id);


}
