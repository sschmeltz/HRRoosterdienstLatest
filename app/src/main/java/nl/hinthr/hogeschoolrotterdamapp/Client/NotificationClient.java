package nl.hinthr.hogeschoolrotterdamapp.Client;


import nl.hinthr.hogeschoolrotterdamapp.model.Notification;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface NotificationClient {
    //put the data in the  model Notification

    @POST("notification")
    Call<Notification> postNotification(@Query("authKey") String authKey,
                                        @Query("name") String name,
                                        @Query("image") String image,
                                        @Query("category") String category,
                                        @Query("description") String description,
                                        @Query("location") String location);
    //put the data in the  model Notification
    @GET("notification")
    Call<Notification> getNotification(@Query("authKey") String authKey, @Query("startDate") String startDate, @Query("endDate") String endDate);

}
