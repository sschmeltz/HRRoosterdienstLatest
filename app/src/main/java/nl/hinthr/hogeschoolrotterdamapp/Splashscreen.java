package nl.hinthr.hogeschoolrotterdamapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;

/**
 * Created by Gebruiker on 15-8-2017.
 */

public class Splashscreen extends AppCompatActivity {
    private int progressBarStatus = 0;
    private Handler handler = new Handler();
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        Thread myThread = new Thread() {
            @Override
            public void run() {
                    while(progressBarStatus < 100){
                        progressBarStatus++;
                        android.os.SystemClock.sleep(20);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setProgress(progressBarStatus);
                            }
                        });
                        }
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(getApplicationContext(), Mainactivity.class);
                                startActivity(intent);
                            }
                        });

            }

        };

        myThread.start();
    }
}
